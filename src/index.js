const task = []
const RENDER_TASK = "render-task"
document.addEventListener("DOMContentLoaded", function(){
    const submitForm = document.getElementById("form-task")
    submitForm.addEventListener("submit", function(event){
        event.preventDefault()
        addTask()
    })


})

function addTask(){
    const nameTask = document.getElementById("task").value
    const deadLine = document.getElementById("time").value
    const status = `<i class="fas fa-minus-circle" style="font-size:18px;color:#eae0c2"></i>`
    const idTask = makeId()
    const generateTask = makeObj(idTask, nameTask, deadLine, status)
    task.push(generateTask)

    document.dispatchEvent(new Event(RENDER_TASK))
}

function makeObj(id, name, time, status){
    return {id, name, time, status}
}

function makeId(){
    return +new Date()
}

document.addEventListener(RENDER_TASK, function(){
    const taskContainer = document.getElementById("render-task")
    taskContainer.innerHTML = ''
    
    for (const taskItem of task){
        const taskElm = makeTask(taskItem)
        taskContainer.append(taskElm)
    }
})

function makeTask(taskObj){
    const rowTask = document.createElement("tr")
    
    const columnStatus = document.createElement("td")
    columnStatus.innerHTML = taskObj.status
    
    const columnName = document.createElement("td")
    columnName.innerText = taskObj.name
    
    const columnDeadline = document.createElement("td")
    columnDeadline.innerText = taskObj.time
    
    const columnAction = document.createElement("td")
    
    const actionUpdate = document.createElement("a")
    actionUpdate.setAttribute("id", "update")
    actionUpdate.setAttribute("onclick", "toggle()")
    actionUpdate.innerHTML = '<i class="fas fa-pencil-alt" style="font-size:18px;color:#eae0c2"></i>'
    actionUpdate.addEventListener("click", function(){
        updateTask(taskObj.id)
    })

    const actionDelete = document.createElement("a")
    actionDelete.setAttribute("id", "delete")
    actionDelete.innerHTML = '<i class="fas fa-trash-alt" style="font-size:18px;color:#eae0c2"></i>'
    actionDelete.addEventListener("click", function(){
        deleteTask(taskObj.id)
    })

    const actionChecked = document.createElement("a")
    actionChecked.setAttribute("id", "checked")
    actionChecked.innerHTML = `<i class="fas fa-check" style="font-size:18px;color:#eae0c2"></i>`
    actionChecked.addEventListener("click", function(){
        checkedTask(taskObj.id)
    })

    columnAction.append( actionChecked, actionUpdate, actionDelete)
    rowTask.append(columnStatus ,columnName, columnDeadline, columnAction)
    return rowTask
}

function deleteTask(id){
    const index = findIndex(id)
    task.splice(index, 1)
    document.dispatchEvent(new Event(RENDER_TASK))
}

function findIndex(id){
    for (const index in task){
        if (task[index].id === id){
            return index
        }
    }
    return -1
}

function updateTask(id){
    const index = findIndex(id)
    const body = document.getElementsByTagName("body")[0]
    
    const sectionUpdate = document.createElement("section")
    sectionUpdate.setAttribute("id", "update-container")

    const formUpdate = document.createElement("form")
    formUpdate.setAttribute("id","form-update")
    
    const divInput1 = document.createElement("div")
    divInput1.classList.add("input")
    const divInput2 = document.createElement("div")
    divInput2.classList.add("input")

    const labelTask = document.createElement("label")
    labelTask.setAttribute("for", "taskUpdate") 
    labelTask.innerText = "Todo"
    const inputTask = document.createElement("input")
    inputTask.setAttribute("id", "taskUpdate")
    inputTask.setAttribute("type", "text")
    inputTask.setAttribute("required", "")
    inputTask.setAttribute("value", `${task[index].name}`)

    const labelTime = document.createElement("label")
    labelTime.setAttribute("for", "timeUpdate") 
    labelTime.innerText = "Deadline"
    const inputTime = document.createElement("input")
    inputTime.setAttribute("id", "timeUpdate")
    inputTime.setAttribute("type", "date")
    inputTime.setAttribute("required", "")
    inputTime.setAttribute("value", `${task[index].time}`)

    const buttonUpdate = document.createElement("button")
    buttonUpdate.classList.add("submit-task")
    buttonUpdate.classList.add("myButton")
    buttonUpdate.setAttribute("type", "submit")
    buttonUpdate.innerText = "Update"

    divInput1.append(labelTask)
    divInput1.append(inputTask)
    divInput2.append(labelTime)
    divInput2.append(inputTime)

    formUpdate.append(divInput1, divInput2, buttonUpdate)
    sectionUpdate.append(formUpdate)
    body.append(sectionUpdate)

    const submitUpdate = document.getElementById("form-update")
    submitUpdate.addEventListener("submit", function(event){
        event.preventDefault()
        updateForm(id)

        setTimeout(() => {
            removeSection()
        }, 300);
    })

}

function checkedTask(id){
    const index = findIndex(id)

    if (task[index].status === `<i class="fas fa-check-circle" style="font-size:18px;color:#eae0c2"></i>`){
        task[index].status = `<i class="fas fa-minus-circle" style="font-size:18px;color:#eae0c2"></i>` 
    } else {
        task[index].status = `<i class="fas fa-check-circle" style="font-size:18px;color:#eae0c2"></i>`
    }
    document.dispatchEvent(new Event(RENDER_TASK))
    
}

function updateForm(id){
    const index = findIndex(id)
    const updateTask = document.getElementById("taskUpdate").value
    const updateTime = document.getElementById("timeUpdate").value
    
    task[index].name = updateTask
    task[index].time = updateTime

    document.dispatchEvent(new Event(RENDER_TASK))
}

function removeSection(){
    const sectionUpdate = document.getElementById("update-container")
    sectionUpdate.remove()

    const containerBlur = document.getElementById("blur")
    containerBlur.classList.remove("active")
}

function toggle(){
    const blur = document.getElementById("blur")
    blur.classList.add("active")
}
